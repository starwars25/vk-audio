//
//  AppDelegate.h
//  VkAudio
//
//  Created by Admin on 12.08.15.
//  Copyright © 2015 Alexander Bogomolov. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property(strong, nonatomic) NSString *token;
@end

