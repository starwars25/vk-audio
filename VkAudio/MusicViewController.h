//
//  MusicViewController.h
//  VkAudio
//
//  Created by Admin on 12.08.15.
//  Copyright © 2015 Alexander Bogomolov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Song.h"
#import "User.h"

@interface MusicViewController : UIViewController
@property(nonatomic, strong) NSString *token;
@property(nonatomic, strong) NSArray *music;

@end
