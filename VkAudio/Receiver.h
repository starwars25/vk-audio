//
//  SongReceiver.h
//  VkAudio
//
//  Created by Admin on 13.08.15.
//  Copyright © 2015 Alexander Bogomolov. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RecieverDelegate

-(void)gotSongs:(NSArray *)songs;
-(void)gotUsers:(NSArray *)user;


@end

@interface Receiver : NSObject <NSURLConnectionDataDelegate>
@property(strong, nonatomic) id<RecieverDelegate> delegate;
-(void)getMusicOfUserWithToken:(NSString *)token;
-(void)getMusicofUserWithID:(NSInteger)ID andToken:(NSString *)token;
-(void)getFriendsWithToken:(NSString *)token;
-(void)getSongsBasedOnRequest:(NSString *)stringRequest withToken:(NSString *)token;

@end
