//
//  FriendsViewController.m
//  VkAudio
//
//  Created by Admin on 12.08.15.
//  Copyright © 2015 Alexander Bogomolov. All rights reserved.
//

#import "FriendsViewController.h"
#import "AppDelegate.h"
#import "Receiver.h"
#import "MusicViewController.h"
#import "FriendsDetailViewController.h"

@interface FriendsViewController () <UITableViewDelegate, UITableViewDataSource, RecieverDelegate>
@property (strong, nonatomic) UITableView *tableView;
@end

@implementation FriendsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self setupView];
//    NSLog(@"%@", self.myMusic);

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    AppDelegate *myDelegate = [[UIApplication sharedApplication] delegate];
    self.token = myDelegate.token;
    
    Receiver *reciever = [[Receiver alloc] init];
    reciever.delegate = self;
    [reciever getFriendsWithToken:self.token];


}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
//    NSLog(@"NavigationBar:\nX: %f, Y: %f, width: %f, height: %f", self.navigationController.navigationBar.frame.origin.x, self.navigationController.navigationBar.frame.origin.y, self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height);
//    NSLog(@"View:\nX: %f, Y: %f, width: %f, height: %f", self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
//    NSLog(@"tableView:\nX: %f, Y: %f, width: %f, height: %f", self.tableView.frame.origin.x, self.tableView.frame.origin.y, self.tableView.frame.size.width, self.tableView.frame.size.height);

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Helper methods

-(void)setupView {
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    // Table View
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    [self.view addSubview:self.tableView];
    [self.tableView reloadData];
    
    // self.tableView constraints
    
    NSLayoutConstraint *topConstraint = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.topLayoutGuide attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0];
    NSLayoutConstraint *bottomConstraint = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.bottomLayoutGuide attribute:NSLayoutAttributeTop multiplier:1.0 constant:0];
    NSLayoutConstraint *leadingConstraint  = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0];
    NSLayoutConstraint *trailingConstraint = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0];
    [self.view addConstraints:@[topConstraint, bottomConstraint, leadingConstraint, trailingConstraint]];
    
    // navigationBar
    
    self.navigationItem.title = @"Friends";
    
}


#pragma mark - TableViewDataSouce
-(NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.users.count;
}
-(UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    User *user = [self.users objectAtIndex:indexPath.row];
    cell.textLabel.text = [user fullName];
    return cell;
}

#pragma mark - TableViewDelegate

-(void)tableView:(nonnull UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    User *selectedUser = [self.users objectAtIndex:indexPath.row];
    FriendsDetailViewController *detail = [[FriendsDetailViewController alloc] init];
    detail.user = selectedUser;
    detail.myMusic = self.myMusic;
    [self.navigationController pushViewController:detail animated:YES];
}

#pragma mark - UserDelegate
-(void)gotUsers:(NSArray *)user {
    self.users = user;
    [self.tableView reloadData];
}
@end
