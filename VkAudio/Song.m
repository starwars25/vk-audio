//
//  Song.m
//  VkAudio
//
//  Created by Admin on 13.08.15.
//  Copyright © 2015 Alexander Bogomolov. All rights reserved.
//

#import "Song.h"

@interface Song() <NSURLConnectionDataDelegate>
@property (strong, nonatomic) NSMutableData *requestData;
@property (nonatomic, copy) void (^completion)(BOOL succeed);
// @property (nonatomic, copy) returnType (^blockName)(parameterTypes);
@end

@implementation Song
-(void)addSongWithToken:(NSString *)token completion:(void (^)(BOOL succeed))completion {
    self.requestData = [[NSMutableData alloc] init];
    NSString *post = [NSString stringWithFormat:@"audio_id=%d&owner_id=%d&access_token=%@", self.aid, self.owner_id, token];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    self.completion = completion;

    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"https://api.vk.com/method/audio.add"]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSURLConnection *connection = [NSURLConnection connectionWithRequest:request delegate:self];

    
    
    
    
    /*    NSString *post = [NSString stringWithFormat:@"need_user=0&count=6000&access_token=%@", token];
     NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
     NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
     NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
     [request setURL:[NSURL URLWithString:@"https://api.vk.com/method/audio.get"]];
     [request setHTTPMethod:@"POST"];
     [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
     [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
     [request setHTTPBody:postData];
     NSURLConnection *connection = [NSURLConnection connectionWithRequest:request delegate:self];
*/
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [self.requestData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"%@", error);
    dispatch_async(dispatch_get_main_queue(), ^{
        self.completion(NO);
    });

    
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:self.requestData options:0 error:nil];
    NSLog(@"%@", json);
    dispatch_async(dispatch_get_main_queue(), ^{
        self.completion(YES);
    });
    //        NSLog(@"%@", json);
}

@end
