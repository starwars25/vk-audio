//
//  MusicViewController.m
//  VkAudio
//
//  Created by Admin on 12.08.15.
//  Copyright © 2015 Alexander Bogomolov. All rights reserved.
//

#import "MusicViewController.h"
#import "LoginViewController.h"
#import "FriendsViewController.h"
#import "AppDelegate.h"
#import "Receiver.h"
#import "MyMusicPlayer.h"
#import "ControlsView.h"


@interface MusicViewController () <UITableViewDataSource, UITableViewDelegate, UITabBarDelegate, RecieverDelegate>
@property (strong, nonatomic) MyMusicPlayer *player;
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) ControlsView *controlsView;

@end

@implementation MusicViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // View Setup
    self.player = [MyMusicPlayer sharedPlayer];

    
    [self setupView];
    
}


-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    AppDelegate *myDelegate = [[UIApplication sharedApplication] delegate];
    self.token = myDelegate.token;
    if (self.token == nil) {
        // login
        LoginViewController *loginViewController = [[LoginViewController alloc] init];
        loginViewController.parentController = self;
        [self presentViewController:loginViewController animated:YES completion:nil];
    } else {
        [self getData];

    }

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
//    NSLog(@"NavigationBar:\nX: %f, Y: %f, width: %f, height: %f", self.navigationController.navigationBar.frame.origin.x, self.navigationController.navigationBar.frame.origin.y, self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height);
//    NSLog(@"View:\nX: %f, Y: %f, width: %f, height: %f", self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
//    NSLog(@"tableView:\nX: %f, Y: %f, width: %f, height: %f", self.tableView.frame.origin.x, self.tableView.frame.origin.y, self.tableView.frame.size.width, self.tableView.frame.size.height);
//    
//    NSLog(@"\n\n\nNavigationControllerView:\nX: %f, Y: %f, width: %f, height: %f", self.navigationController.view.frame.origin.x, self.navigationController.view.frame.origin.y, self.navigationController.view.frame.size.width, self.navigationController.view.frame.size.height);

    

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Helper methods

- (void)setupView {
    self.view.backgroundColor = [UIColor whiteColor];
    
    //    self.view.translatesAutoresizingMaskIntoConstraints = NO;
    
    // tableViewSetup
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    //    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    [self.tableView reloadData];
    [self.view addSubview:self.tableView];
    
    // Controls View Setup
    
    self.controlsView = [[ControlsView alloc] init];
    self.controlsView.translatesAutoresizingMaskIntoConstraints = NO;
    self.controlsView.player = self.player;
//    self.controlsView.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:self.controlsView];
    
    
    
    
    // Constraints setup
    
    //tableView
    
//    NSLayoutConstraint *tableViewTopConstraint = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.controlsView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0];
    NSLayoutConstraint *tableViewTopConstraint;
        tableViewTopConstraint = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.controlsView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0];
    NSLayoutConstraint *tableViewBottomConstraint = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.bottomLayoutGuide attribute:NSLayoutAttributeTop multiplier:1.0 constant:0];
    NSLayoutConstraint *tableViewLeadingConstraint = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0];
    NSLayoutConstraint *tableViewTrailingConstraint = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0];
    [self.view addConstraints:@[tableViewTopConstraint, tableViewBottomConstraint, tableViewTrailingConstraint, tableViewLeadingConstraint]];
    
    
    
    // Controls View
    
    NSLayoutConstraint *controlsViewTopConstraint = [NSLayoutConstraint constraintWithItem:self.controlsView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.topLayoutGuide attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0];
    NSLayoutConstraint *controlsViewLeadingConstraint = [NSLayoutConstraint constraintWithItem:self.controlsView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0];
    NSLayoutConstraint *controlsViewTrailingConstraint = [NSLayoutConstraint constraintWithItem:self.controlsView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0];
    NSLayoutConstraint *controlsViewHeight = [NSLayoutConstraint constraintWithItem:self.controlsView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:30];
    [self.view addConstraints:@[controlsViewHeight, controlsViewLeadingConstraint, controlsViewTopConstraint, controlsViewTrailingConstraint]];
    
    // playButton
    
}

- (void)getData {
    Receiver *receiver = [[Receiver alloc] init];
    receiver.delegate = self;
        [receiver getMusicOfUserWithToken:self.token];
}



#pragma mark - UITableViewDataSource

-(NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.music.count;
}

-(UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    UITableViewCell *cell= [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell==nil) {
        // you cans set subtitle using this way
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
    }
    Song *song = [self.music objectAtIndex:indexPath.row];
    cell.textLabel.text = song.title;
    cell.detailTextLabel.text = song.artist;
    
    return cell;
//    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
//            Song *song = [self.music objectAtIndex:indexPath.row];
//            cell.textLabel.text = song.title;
//            cell.detailTextLabel.text = song.artist;

    }

#pragma mark - SongDelegate
-(void)gotSongs:(NSArray *)songs {
    self.music = songs;
    UINavigationController *navigation = (UINavigationController *)[self.tabBarController.viewControllers objectAtIndex:1];
    FriendsViewController *friendsViewController = (FriendsViewController *)[navigation.viewControllers objectAtIndex:0];
    NSMutableArray *myMusic = [[NSMutableArray alloc] init];
    for (Song *song in songs) {
        [myMusic addObject:song.title];
    }
    friendsViewController.myMusic = myMusic;

    [self.tableView reloadData];
}

#pragma mark - Actions
-(void)playPressed {
    [self.player resume];
}

-(void)pausePressed {
    [self.player pause];
}

#pragma mark - UITableViewDelegate

-(void)tableView:(nonnull UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    Song *selectedSong = [self.music objectAtIndex:indexPath.row];
//    self.trackTitleLabel.text = selectedSong.title;
    [self.controlsView setTitleLabel:selectedSong.title];
   
    // TODO: change to grand central dispatch GCD
    
    queue.name = @"song getting queue";
    [queue addOperationWithBlock:^{
        
        NSData *songData = [NSData dataWithContentsOfURL:selectedSong.url];
        [self.player playWithData:songData];
    }];
    
}


@end
