//
//  SongReceiver.m
//  VkAudio
//
//  Created by Admin on 13.08.15.
//  Copyright © 2015 Alexander Bogomolov. All rights reserved.
//

#import "Receiver.h"
#import "Song.h"
#import "User.h"
@interface Receiver() <NSURLConnectionDataDelegate>
@property (strong, nonatomic) NSMutableData *requestData;
@property (strong, nonatomic) NSString *type;

@end

@implementation Receiver
-(void)getMusicofUserWithID:(NSInteger)ID andToken:(NSString *)token {
    self.type = @"music";
    self.requestData = [[NSMutableData alloc] init];
    NSString *post = [NSString stringWithFormat:@"owner_id=%ld&need_user=0&count=6000&access_token=%@", (long)ID, token];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"https://api.vk.com/method/audio.get"]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSURLConnection *connection = [NSURLConnection connectionWithRequest:request delegate:self];

}
-(void)getMusicOfUserWithToken:(NSString *)token {
    self.requestData = [[NSMutableData alloc] init];
    self.type = @"music";
    
    NSString *post = [NSString stringWithFormat:@"need_user=0&count=6000&access_token=%@", token];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"https://api.vk.com/method/audio.get"]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSURLConnection *connection = [NSURLConnection connectionWithRequest:request delegate:self];

}

-(void)getFriendsWithToken:(NSString *)token {
    self.type = @"friends";
    self.requestData = [[NSMutableData alloc] init];
    NSString *post = [NSString stringWithFormat:@"order=hints&fields=nickname&name_case=nom&access_token=%@", token];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"https://api.vk.com/method/friends.get"]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSURLConnection *connection = [NSURLConnection connectionWithRequest:request delegate:self];

}

-(void)getSongsBasedOnRequest:(NSString *)stringRequest withToken:(NSString *)token {
    self.requestData = [[NSMutableData alloc] init];
    self.type = @"music";
    
    NSString *post = [NSString stringWithFormat:@"q=%@&count=300&access_token=%@", stringRequest, token];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"https://api.vk.com/method/audio.search"]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSURLConnection *connection = [NSURLConnection connectionWithRequest:request delegate:self];

    
}

#pragma mark - NSURLConnectionDataDelegate

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [self.requestData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"%@", error);
    
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection {
    @try {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:self.requestData options:0 error:nil];
        //        NSLog(@"%@", json);
        if ([self.type isEqualToString:@"music"]) {
            NSMutableArray *music = [[NSMutableArray alloc] init];
            NSArray *audioArray = [json objectForKey:@"response"];
            for (NSDictionary *audioDictionary in audioArray) {
                @try {
                    Song *song = [[Song alloc] init];
                    song.aid = [[audioDictionary objectForKey:@"aid"] unsignedIntegerValue];
                    song.artist = [audioDictionary objectForKey:@"artist"];
                    //        song.duration = [[audioDictionary objectForKey:@"duration"] unsignedIntegerValue];
                    //        song.genre = [[audioDictionary objectForKey:@"genre"] unsignedIntegerValue];
                    //        song.lyrics_id = [audioDictionary objectForKey:@"lyrics_id"];
                    song.owner_id = [[audioDictionary objectForKey:@"owner_id"] unsignedIntegerValue];
                    song.title = [audioDictionary objectForKey:@"title"];
                    song.url = [NSURL URLWithString:[audioDictionary objectForKey:@"url"]];
                    [music addObject:song];
                } @catch(NSException *e) {
                    
                }
            }
            [self.delegate gotSongs:music];
            self.requestData = nil;
        } else if ([self.type isEqualToString:@"friends"]) {
            NSMutableArray *friends = [[NSMutableArray alloc] init];
            NSArray *friendsArray = [json objectForKey:@"response"];
            for (NSDictionary *friendsDictionary in friendsArray) {
                User *user = [[User alloc] init];
                user.firstName = [friendsDictionary objectForKey:@"first_name"];
                user.lastName = [friendsDictionary objectForKey:@"last_name"];
                user.nickname = [friendsDictionary objectForKey:@"nickname"];
                user.online = [[friendsDictionary objectForKey:@"online"] integerValue];
                user.uid = [[friendsDictionary objectForKey:@"uid"] integerValue];
                user.userId = [[friendsDictionary objectForKey:@"user_id"] integerValue];
                [friends addObject:user];
                
            }
            [self.delegate gotUsers:friends];
            self.requestData = nil;
            
        }

    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}


@end
