//
//  FriendsDetailViewController.m
//  VkAudio
//
//  Created by Admin on 14.08.15.
//  Copyright © 2015 Alexander Bogomolov. All rights reserved.
//

#import "FriendsDetailViewController.h"
#import "ControlsView.h"
#import "MyMusicPlayer.h"
#import "Song.h"
#import "Receiver.h"
#import "AppDelegate.h"
@interface FriendsDetailViewController () <UITableViewDataSource, RecieverDelegate, UITableViewDelegate, UIGestureRecognizerDelegate>
@property (strong, nonatomic) ControlsView *controlsView;
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) MyMusicPlayer *player;
@property (strong, nonatomic) NSMutableArray *selectedSongs;
@property (assign, nonatomic) BOOL sending;
@end

@implementation FriendsDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.player = [MyMusicPlayer sharedPlayer];
    self.selectedSongs = [[NSMutableArray alloc] init];
    [self setupView];
    UILongPressGestureRecognizer *gester = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longTap:)];
    gester.minimumPressDuration = 2.0;
    gester.delegate = self;
    [self.tableView addGestureRecognizer:gester];
    self.sending = NO;

//    NSLog(@"%@", self.myMusic);
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated {
//    self.navigationController.navigationBarHidden = YES;
    [super viewWillAppear:animated];
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    self.token = delegate.token;
    Receiver *receiver = [[Receiver alloc] init];
    receiver.delegate = self;
    [receiver getMusicofUserWithID:self.user.userId andToken:self.token];
    
    
}
-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    NSLog(@"X: %f, Y: %f, width: %f, height: %f", self.tableView.frame.origin.x, self.tableView.frame.origin.y, self.tableView.frame.size.width, self.tableView.frame.size.height);
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Helper methods
-(void)setupView {
    self.view.backgroundColor = [UIColor whiteColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.tableView = [[UITableView alloc] init];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.tableView];
    
    
    self.controlsView = [[ControlsView alloc] init];
    self.controlsView.player = self.player;
    self.controlsView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.controlsView];
    [self setControlsViewConstraints];
    [self setTableViewConstraints];
}

-(void)setTableViewConstraints {
    NSLayoutConstraint *topConstraint = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.controlsView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0];
    NSLayoutConstraint *leadingConstraint = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0];
    NSLayoutConstraint *trailingConstraint = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0];
    NSLayoutConstraint *bottomConstraint = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.bottomLayoutGuide attribute:NSLayoutAttributeTop multiplier:1.0 constant:0];
    [self.view addConstraints:@[topConstraint, leadingConstraint, trailingConstraint, bottomConstraint]];
}

-(void)setControlsViewConstraints {
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:self.controlsView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.topLayoutGuide attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0];
    NSLayoutConstraint *leading = [NSLayoutConstraint constraintWithItem:self.controlsView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0];
    NSLayoutConstraint *trailing = [NSLayoutConstraint constraintWithItem:self.controlsView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0];
    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:self.controlsView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:30];
    [self.view addConstraints:@[top, leading, trailing, height]];
}

-(UILabel *)setupAccessoryLabel {
    UILabel *label = [[UILabel alloc] init];
    label.text = @"Added";
    [label sizeToFit];
    label.textColor = [UIColor lightGrayColor];
    return label;
}

-(BOOL)containsSong:(Song *)song {
    for (NSString *string in self.myMusic) {
        if ([string isEqualToString:song.url.absoluteString]) {
            return YES;
        }
        
    }
    return NO;
}

#pragma mark - UITableViewDataSource
-(NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.music.count;
}

-(UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
    }
    Song *song = [self.music objectAtIndex:indexPath.row];
    cell.textLabel.text = song.title;
    cell.detailTextLabel.text = song.artist;
    if ([self.myMusic containsObject:song.title]) {
        cell.accessoryView = [self setupAccessoryLabel];
    } else {
        cell.accessoryView = nil;
    }
    return cell;
}

#pragma mark - ReceiverDelegate
-(void)gotSongs:(NSArray *)songs {
    self.music = songs;
    [self.tableView reloadData];
}

#pragma mark - TableViewDelegate
-(void)tableView:(nonnull UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    Song *selectedSong = [self.music objectAtIndex:indexPath.row];
    //    self.trackTitleLabel.text = selectedSong.title;
    [self.controlsView setTitleLabel:selectedSong.title];
    
    // TODO: change to grand central dispatch GCD
    
    queue.name = @"song getting queue";
    [queue addOperationWithBlock:^{
        
        NSData *songData = [NSData dataWithContentsOfURL:selectedSong.url];
        [self.player playWithData:songData];
    }];

}

#pragma mark - Actions
-(void)longTap:(UILongPressGestureRecognizer*)sender {
    if (sender.state == UIGestureRecognizerStateEnded) {
        CGPoint p = [sender locationInView:self.tableView];
        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:p];
        
        Song *song = [self.music objectAtIndex:indexPath.row];
        NSLog(@"%@", song.title);
        if (![self.music containsObject:song.title]) {
            // add audio
            
            [song addSongWithToken:self.token completion:^(BOOL succeed) {
                if (succeed) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Success!" message:@"Audio added." delegate:nil cancelButtonTitle:nil otherButtonTitles: nil];
                    [alertView show];
                    [self.myMusic addObject:song.title];
                    [self.tableView reloadData];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [alertView dismissWithClickedButtonIndex:0 animated:YES];
                    });

                }
                
            }];
            
        }
        

    }
}


@end
