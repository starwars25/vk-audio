//
//  MyMusicPlayer.h
//  VkAudio
//
//  Created by Admin on 13.08.15.
//  Copyright © 2015 Alexander Bogomolov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>

@interface MyMusicPlayer : NSObject
+(instancetype)sharedPlayer;

-(void)playWithData:(NSData *)data;
-(void)pause;
-(void)resume;

@end
