//
//  MyMusicPlayer.m
//  VkAudio
//
//  Created by Admin on 13.08.15.
//  Copyright © 2015 Alexander Bogomolov. All rights reserved.
//

#import "MyMusicPlayer.h"

@interface MyMusicPlayer ()
@property (nonatomic, strong) AVAudioPlayer *player;
@property (nonatomic, assign) BOOL isPlaying;

@end

@implementation MyMusicPlayer
static MyMusicPlayer *_musicPlayer;
+(instancetype)sharedPlayer {
    @synchronized([MyMusicPlayer class]) {
        if (_musicPlayer == nil) {
            _musicPlayer = [[MyMusicPlayer alloc] init];
        }
        return _musicPlayer;
    }
    return nil;
}
-(void)playWithData:(NSData *)data {
        self.player = [[AVAudioPlayer alloc] initWithData:data error:nil];
        [self.player play];
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
        [[AVAudioSession sharedInstance] setActive: YES error: nil];
        [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];

        self.isPlaying = YES;
    
    
}
-(void)pause {
    if (self.isPlaying) {
        
    
        [self.player pause];
        self.isPlaying = NO;
    }
    
}
-(void)resume {
    if (self.isPlaying) {
        
    } else {
        [self.player play];
        self.isPlaying = YES;
    }
}
-(void)stop {
    
}
-(instancetype)init {
    self = [super init];
    if (self) {
        self.isPlaying = NO;
    }
    return self;
}
@end
