//
//  FriendsViewController.h
//  VkAudio
//
//  Created by Admin on 12.08.15.
//  Copyright © 2015 Alexander Bogomolov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@interface FriendsViewController : UIViewController
@property (nonatomic, strong) NSString *token;
@property (nonatomic, strong) NSArray *users;
@property (nonatomic, strong) NSMutableArray *myMusic;
@end
