//
//  ControlsView.h
//  VkAudio
//
//  Created by Admin on 14.08.15.
//  Copyright © 2015 Alexander Bogomolov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyMusicPlayer.h"

@interface ControlsView : UIView
@property (strong, nonatomic) MyMusicPlayer *player;

-(void)setTitleLabel:(NSString *)value;
@end
