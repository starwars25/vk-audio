//
//  User.h
//  VkAudio
//
//  Created by Admin on 13.08.15.
//  Copyright © 2015 Alexander Bogomolov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject
@property(strong, nonatomic) NSString *firstName;
@property(strong, nonatomic) NSString *lastName;
@property(strong, nonatomic) NSString *nickname;
@property(assign, nonatomic) NSInteger online;
@property(assign, nonatomic) NSInteger uid;
@property(assign, nonatomic) NSInteger userId;


-(NSString *)fullName;

@end
