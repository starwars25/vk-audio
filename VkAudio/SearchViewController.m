//
//  SearchViewController.m
//  VkAudio
//
//  Created by Admin on 16.08.15.
//  Copyright © 2015 Alexander Bogomolov. All rights reserved.
//

#import "SearchViewController.h"
#import "Receiver.h"
#import "AppDelegate.h"
#import "ControlsView.h"
#import "Song.h"
#import "MyMusicPlayer.h"

@interface SearchViewController () <UITableViewDataSource, UITableViewDelegate, RecieverDelegate>
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) UITextField *textFieldRequest;
@property (strong, nonatomic) ControlsView *controlsView;
@property (strong, nonatomic) Receiver *receiver;
@property (strong, nonatomic) MyMusicPlayer *player;
@property (strong, nonatomic) NSArray *music;
@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    self.token = delegate.token;
    self.player = [MyMusicPlayer sharedPlayer];

    
    [self setupView];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - Helper methods
-(void)setupView {
    self.view.backgroundColor = [UIColor whiteColor];
    
    // setup tableView
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    
    // setup textField
    
    
    self.textFieldRequest = [[UITextField alloc] init];
    self.textFieldRequest.translatesAutoresizingMaskIntoConstraints = NO;
    self.textFieldRequest.placeholder = @"Enter song title or artist";
    self.textFieldRequest.backgroundColor = [UIColor lightGrayColor];
    [self.textFieldRequest addTarget:self action:@selector(textFieldRequestDidChange) forControlEvents:UIControlEventEditingChanged];
    [self.view addSubview:self.textFieldRequest];
    
    // setupControlsView
    
    self.controlsView = [[ControlsView alloc] init];
    self.controlsView.translatesAutoresizingMaskIntoConstraints = NO;
    self.controlsView.player = self.player;
    //    self.controlsView.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:self.controlsView];

    [self setupControlsViewConstraints];
    [self setupTableViewConstraints];
    [self setupTextFieldConstraints];
}

-(void)setupControlsViewConstraints {
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:self.controlsView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.topLayoutGuide attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0];
    NSLayoutConstraint *leading = [NSLayoutConstraint constraintWithItem:self.controlsView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0];
    NSLayoutConstraint *trailing = [NSLayoutConstraint constraintWithItem:self.controlsView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0];
    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:self.controlsView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:30];
    [self.view addConstraints:@[top, leading, trailing, height]];

}

-(void)setupTableViewConstraints {
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.textFieldRequest attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0];
    NSLayoutConstraint *leading = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0];
    NSLayoutConstraint *trailing = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0];
    NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.bottomLayoutGuide attribute:NSLayoutAttributeTop multiplier:1.0 constant:0];
    [self.view addConstraints:@[top, leading, trailing, bottom]];

}

-(void)setupTextFieldConstraints {
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:self.textFieldRequest attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.controlsView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0];
    NSLayoutConstraint *leading = [NSLayoutConstraint constraintWithItem:self.textFieldRequest attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0 constant:10.0];
    NSLayoutConstraint *trailing = [NSLayoutConstraint constraintWithItem:self.textFieldRequest attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:-10.0];
    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:self.textFieldRequest attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:30];
    [self.view addConstraints:@[top, leading, trailing, height]];

}

#pragma mark - UITableViewDataSource

-(NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.music.count;
}
-(UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
        
    }
    Song *song = [self.music objectAtIndex:indexPath.row];
    cell.textLabel.text = song.title;
    cell.detailTextLabel.text = song.artist;
    return cell;
}

#pragma mark - Actions
-(void)textFieldRequestDidChange {
//    NSLog(@"%@", self.textFieldRequest.text);
    [self.receiver getSongsBasedOnRequest:self.textFieldRequest.text withToken:self.token];
    
    // Get songs based on string request
    // Store them in property
    // Update table view
}

#pragma mark - Overriding
-(Receiver *)receiver {
    if (_receiver == nil) {
        _receiver = [[Receiver alloc] init];
        _receiver.delegate = self;
    }
    return _receiver;
}

#pragma mark - RecieverDelegate
-(void)gotSongs:(NSArray *)songs {
    self.music = songs;
    [self.tableView reloadData];
}

#pragma mark - UITableViewDelegate

-(void)tableView:(nonnull UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    Song *selectedSong = [self.music objectAtIndex:indexPath.row];
    //    self.trackTitleLabel.text = selectedSong.title;
    [self.controlsView setTitleLabel:selectedSong.title];
    
    // TODO: change to grand central dispatch GCD
    
    queue.name = @"song getting queue";
    [queue addOperationWithBlock:^{
        
        NSData *songData = [NSData dataWithContentsOfURL:selectedSong.url];
        [self.player playWithData:songData];
    }];
    
}
@end
