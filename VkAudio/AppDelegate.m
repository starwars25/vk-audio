//
//  AppDelegate.m
//  VkAudio
//
//  Created by Admin on 12.08.15.
//  Copyright © 2015 Alexander Bogomolov. All rights reserved.
//

#import "AppDelegate.h"
#import "MusicViewController.h"
#import "FriendsViewController.h"
#import "SearchViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

/*
    TODO:
 
    1. Implement adding. - DONE
    2. Implement search.
    3. Implement sending (With separate sending modal view).
    4. Implement removing with slide to delete.
    5. Improve UI.
 
 
 
 */


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self.window makeKeyAndVisible];
    UITabBarController *tabBarController = [[UITabBarController alloc] init];
    self.window.rootViewController = tabBarController;
    MusicViewController *musicViewController = [[MusicViewController alloc] init];
    FriendsViewController *friendsViewController = [[FriendsViewController alloc] init];
    musicViewController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Music" image:[UIImage imageNamed:@"Musical Notes-50"] selectedImage:nil];
    friendsViewController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Friends" image:[UIImage imageNamed:@"User Group-50"] selectedImage:nil];
    UINavigationController *friendsNavigationController = [[UINavigationController alloc] initWithRootViewController:friendsViewController];
    SearchViewController *searchViewController = [[SearchViewController alloc] init];
    searchViewController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Search" image:[UIImage imageNamed:@"Search-50"] selectedImage:nil];
    tabBarController.viewControllers = @[musicViewController, friendsNavigationController, searchViewController];
//    tabBarController.tabBar.translatesAutoresizingMaskIntoConstraints = NO;
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
