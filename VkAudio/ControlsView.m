//
//  ControlsView.m
//  VkAudio
//
//  Created by Admin on 14.08.15.
//  Copyright © 2015 Alexander Bogomolov. All rights reserved.
//

#import "ControlsView.h"

@interface ControlsView ()

@property (strong, nonatomic) UIButton *playButton;
@property (strong, nonatomic) UIButton *pauseButton;
@property (strong, nonatomic) UILabel *trackTitleLabel;


@end
@implementation ControlsView


-(void)layoutSubviews {
    [self setupView];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)setTitleLabel:(NSString *)value {
    self.trackTitleLabel.text = value;
}

#pragma mark - Actions
-(void)playPressed {
    [self.player resume];
}

-(void)pausePressed {
    [self.player pause];
}

#pragma mark - Helper methods

- (void)setupView {
    // playButton setup
    if (self.playButton == nil && self.pauseButton == nil && self.trackTitleLabel == nil) {
        self.backgroundColor = [UIColor whiteColor];
        
        self.playButton = [UIButton buttonWithType:UIButtonTypeSystem];
        [self.playButton setTitle:@"Play" forState:UIControlStateNormal];
        [self.playButton sizeToFit];
        self.playButton.translatesAutoresizingMaskIntoConstraints = NO;
        [self.playButton addTarget:self action:@selector(playPressed) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.playButton];
        
        // pauseButton setup
        
        self.pauseButton = [UIButton buttonWithType:UIButtonTypeSystem];
        [self.pauseButton setTitle:@"Pause" forState:UIControlStateNormal];
        [self.pauseButton sizeToFit];
        self.pauseButton.translatesAutoresizingMaskIntoConstraints = NO;
        [self.pauseButton addTarget:self action:@selector(pausePressed) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:self.pauseButton];
        
        // trackTitleLabel
        
        self.trackTitleLabel = [[UILabel alloc] init];
        self.trackTitleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.trackTitleLabel.text = @"Track Title";
        [self.trackTitleLabel sizeToFit];
        //    self.trackTitleLabel.backgroundColor = [UIColor whiteColor];
        self.trackTitleLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.trackTitleLabel];
        
        
        // playButton
        
        NSLayoutConstraint *playButtonYConstraint = [NSLayoutConstraint constraintWithItem:self.playButton attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0];
        NSLayoutConstraint *playButtonLeading = [NSLayoutConstraint constraintWithItem:self.playButton attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeading multiplier:1.0 constant:15];
        NSLayoutConstraint *playButtonWidth = [NSLayoutConstraint constraintWithItem:self.playButton attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:self.playButton.frame.size.width];
        NSLayoutConstraint *playButtonHeight = [NSLayoutConstraint constraintWithItem:self.playButton attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:self.playButton.frame.size.height];
        
        [self addConstraints:@[playButtonYConstraint, playButtonLeading, playButtonWidth, playButtonHeight]];
        
        // pauseButton
        
        NSLayoutConstraint *pauseButtonYConstraint = [NSLayoutConstraint constraintWithItem:self.pauseButton attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0];
        NSLayoutConstraint *pauseButtonLeading = [NSLayoutConstraint constraintWithItem:self.pauseButton attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.playButton attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:15];
        NSLayoutConstraint *pauseButtonWidth = [NSLayoutConstraint constraintWithItem:self.pauseButton attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:self.pauseButton.frame.size.width];
        NSLayoutConstraint *pauseButtonHeight = [NSLayoutConstraint constraintWithItem:self.pauseButton attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:self.pauseButton.frame.size.height];
        
        [self addConstraints:@[pauseButtonYConstraint, pauseButtonLeading, pauseButtonWidth, pauseButtonHeight]];
        
        // trackTitleLabel
        
        NSLayoutConstraint *trackTitleLabelYConstraint = [NSLayoutConstraint constraintWithItem:self.trackTitleLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0];
        NSLayoutConstraint *trackTitleLabelLeading = [NSLayoutConstraint constraintWithItem:self.trackTitleLabel attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.pauseButton attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:15];
        NSLayoutConstraint *trackTitleLabelTrailing = [NSLayoutConstraint constraintWithItem:self.trackTitleLabel attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:-15];
        
        [self addConstraints:@[trackTitleLabelYConstraint, trackTitleLabelLeading, trackTitleLabelTrailing]];
    }
}


@end
