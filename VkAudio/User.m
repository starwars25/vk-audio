//
//  User.m
//  VkAudio
//
//  Created by Admin on 13.08.15.
//  Copyright © 2015 Alexander Bogomolov. All rights reserved.
//

#import "User.h"

@implementation User
-(NSString *)fullName {
    return [NSString stringWithFormat:@"%@ %@", self.firstName, self.lastName];
}

@end
