//
//  FriendsDetailViewController.h
//  VkAudio
//
//  Created by Admin on 14.08.15.
//  Copyright © 2015 Alexander Bogomolov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@interface FriendsDetailViewController : UIViewController
@property (strong, nonatomic) User *user;
@property (strong, nonatomic) NSArray *music;
@property (strong, nonatomic) NSMutableArray *myMusic;
@property (strong, nonatomic) NSString *token;
@end
