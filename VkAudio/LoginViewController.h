//
//  LoginViewController.h
//  VkAudio
//
//  Created by Admin on 12.08.15.
//  Copyright © 2015 Alexander Bogomolov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MusicViewController.h"

@interface LoginViewController : UIViewController
@property(nonatomic, strong) MusicViewController *parentController;
@end
