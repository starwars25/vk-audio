//
//  Song.h
//  VkAudio
//
//  Created by Admin on 13.08.15.
//  Copyright © 2015 Alexander Bogomolov. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface Song : NSObject
@property(assign, nonatomic) NSUInteger aid;
@property(strong, nonatomic) NSString *artist;
@property(assign, nonatomic) NSUInteger duration;
@property(assign, nonatomic) NSUInteger genre;
@property(strong, nonatomic) NSString *lyrics_id;
@property(assign, nonatomic) NSUInteger owner_id;
@property(strong, nonatomic) NSString *title;
@property(strong, nonatomic) NSURL *url;

-(void)addSongWithToken:(NSString *)token completion:(void (^)(BOOL succeed))completion;

// +(void)imageForPhoto:(NSDictionary *)photo size:(NSString *)size completion:(void (^)(UIImage *))completion {


@end
